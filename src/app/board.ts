/**
 * Board component constructs and solves grid using Cell and Unit classes
 */

import Cell from './cell';
import Unit from './unit';

export class Board {
    data: String = '';
    units: Array<Unit> = [];  // rows, cols, and boxes
    cells: Array<Cell> = []; // individual cells
    time: any; // solve time measurements
    length = 9 * 9; // type of grid

    constructor() {
        // create grid with cells and units
        this.cells = this.createCells();
        this.units = this.crateUnits(this.cells);
        this.setCellUnits(this.cells, this.units);
    }

    clearData() {
        this.cells.forEach((c: Cell) => {
            c.value = null;
            c.candidates = [];
            c.tempCandidates = [];
        });
    }

    setData(data: String) {
        this.data = data;
        //
        if (typeof data !== 'string') {
            throw new Error('Expected string for data value!');
        }

        if (data.length !== this.length) {
            throw new Error('Grid data is invalid!');
        }
        // assign cell values;
        this.cells.forEach((c: Cell, i: number) => c.setValue(parseInt(data.substr(i, 1), 10)));
    }

    /**
     * Created Units of Row,Col,Box with cells of sudoku board
     * @param cells Collection of Cells
     * @returns Array<Unit>
     */
    crateUnits(cells: Array<Cell>) {
        let rows: Array<Cell> = [];
        const cols: Array<any> = [];
        const boxes: Array<any> = [];
        const units: Array<Unit> = [];

        // create rows/cols/boxes
        // we'll try and do it in one iteration of loop to save time and cpu
        cells.forEach((cell, i) => {
            // for each 9 iterations - create row Unit
            if (rows.length === 9) {
                //  create new row unit then push to row collection
                units.push(new Unit(rows, 'row', Math.floor(i / 9)));
                // then reset the collection
                rows = [];
            }

            // push to collection
            // when it fills up to 9 a row unit will be created
            rows.push(cell);

            // while at the same time create column units arrays
            // we'll repeat column assoc array every 9 iterations
            // so that all values belong to their rows
            const colID = i % 9;
            // create column by id if not already there
            cols[colID] = cols[colID] || new Array();
            // add a cell to its column
            cols[colID].push(cell);

            // create boxes 3x3
            // a tricky formula to have box id repeats for a given box till all
            // of its constituent cell are added
            const boxID = Math.floor((i % 9) / 3) + (3 * Math.floor(i / 27) || 0);
            //  console.log(boxID)
            // create box array if not already there
            boxes[boxID] = boxes[boxID] || new Array();
            // add cell to box collection
            boxes[boxID].push(cell);
        });

        // set last row
        units.push(new Unit(rows, 'row', 9));

        // set col
        cols.forEach((cell: Array<Cell>, i: number) => {
            units.push(new Unit(cell, 'col', i));
        });

        // set all boxes
        boxes.forEach((cell: Array<Cell>, i: number) => {
            units.push(new Unit(cell, 'box', i));
        });
        //
        return units;
    }

    /**
     * Sets each cell with its peer units as per sudoku rules
     * @param cells Array of Cells
     * @param units Array of Units
     * @returns Void
     */
    setCellUnits(cells: Array<Cell>, units: Array<Unit>) {
        // for each cell - connect it to its peer units
        cells.forEach(cell => {
            // traverse units looking for cell peers
            units.forEach(unit => {
                if (unit.hasCell(cell)) {
                    cell.addUnit(unit);
                }
            });
        });
        // now we have connected cells to its peer units
        // and units having all its cells such as rows, cols, boxes
    }

    getBoxUnits() {
        return this.units.filter(unit => unit.type === 'box');
    }

    /**
     * Parses String of values and creates array of Cells
     * @param data String of values as per exercise parameters
     * @returns Array of cells
     */
    createCells() {
        const collection: Array<Cell> = [];
        for (let i = 0; i < this.length; i += 1) {
            collection.push(new Cell(i));
        }

        return collection;
    }

    /**
     * Tries to solve give data set
     * Loops over cell collection calling solveCell() on each
     */
    solve() {
        this.time = performance.now();
        //  calculate candidates for each cell according to initial grid - only do it once
        this.setCandidates();
        //  sort the cell with least possibilities first
        const sortedCells = this.sortCellsByCandidates(this.cells);

        for (let j = 0; j < sortedCells.length; j += 1) {
            const cell = sortedCells[j];
            const result = this.solveCell(cell);

            if (result === false) {
                console.log(j);

                // sortedCells.forEach(c => c.resetCandidates());
                // cell.resetCandidates();
                let i = 1;
                let _break = false;

                while (j - i >= 0 && _break !== true) {
                    const prevCell = sortedCells[j - i];
                    // prevCell.resetCandidates();
                    //
                    if (prevCell.candidates.length > prevCell.candidateIndex + 1) {
                        prevCell.resetCandidates();
                        prevCell.candidateIndex += 1; // move pointer further
                        prevCell.tempCandidates = prevCell.candidates.slice(prevCell.candidateIndex);
                        // solve previous sell again
                        // j -= i + 1;
                        j = 0;
                        _break = true;
                    } else {
                        // prevCell.candidateIndex = 0;
                        i += 1;
                    }
                }

            } else {
                // sortedCells[j].value = sortedCells[j].tempCandidates[0];
                console.log('found');
            }
        }

        this.time = performance.now() - this.time;

        let solved = true;

        // sortedCells.forEach(c => c.tempCandidates = c.candidates.slice(c.candidateIndex, 1));

        this.cells.forEach(cell => {
            if (cell.value < 1 && cell.tempCandidates.length > 1) {
                solved = false;
            } else {
                cell.value = cell.value || cell.tempCandidates[0];
            }
        });

        this.units.forEach(unit => {
            if (unit.isFull() === false) {
                solved = false;
            }
        });

        if (solved === true) {
            console.log(`Pass t: ${this.time}ms`);
        } else {
            console.error(`Failed t: ${this.time}ms`);
        }
    }

    /**
     * Tries to resolve single cell candidates by reducing it to one value
     */
    solveCell(cell: Cell) {
        let solved: Boolean = true;

        if (cell.tempCandidates.length === 0) {
            return false;
        }

        for (let j = 0; j < this.cells.length; j += 1) {
            if (this.cells[j].isLastCandidate() === false) {
                solved = false;
            }
        }

        if (solved === true) {
            return true;
        }

        for (let i = 0; i < cell.tempCandidates.length; i += 1) {

            const value = cell.tempCandidates[i];
            const result = cell.assignPossibleCandidate(value);
            //
            if (result === true) {
                return true;
            } else {
                // cell.resetCandidates();
            }

            // else continue going over remaining tempCandidates
            // cell.resetCandidates();
        }

        // not solved
        return false;
    }

    /**
     * cet all cells candidates as possible values
     * where values are reduced to possibilities of all cell units
     * excluding values that are not possible in any unit
     * @param cells Array of cells
     */
    setCandidates() {
        // calculate candidates for each cell
        // get cells with no candidates
        this.cells.forEach(cell => cell.setCandidates());
    }

    /**
     * Filters cell array to exclude cells with only one possible value
     * As well as sorts resulting array to list starting with fewer possibilities cells
     * @param cells Array of cells
     */
    sortCellsByCandidates(cells: Array<Cell>) {
        //  sort the cell with least possibilities first
        return cells
            .filter(cell => cell.candidates.length > 1)
            .sort((a, b) => a.candidates.length - b.candidates.length);
    }
}