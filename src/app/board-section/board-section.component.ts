import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-board-section',
  templateUrl: './board-section.component.html',
  styleUrls: ['./board-section.component.scss']
})

export class BoardSectionComponent implements OnInit {
  @Input() unit = null;

  constructor() { }

  ngOnInit( ) {
    
  }  
}
