import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-board-cell',
  templateUrl: './board-cell.component.html',
  styleUrls: ['./board-cell.component.scss']
})
export class BoardCellComponent implements OnInit {
  @Input() cell = null;
  value: string ;
  position: number;

  constructor() {
    // this.value = this.data === 0 ? '' : this.data;  
  }

  ngOnInit() {
        this.value = this.cell.value || '';
        this.position = this.cell.position;
  }

  viewTitle(event:any){
      if(!this.cell.tempCandidates){
          return '';
      }
    event.target.setAttribute('title', this.cell.tempCandidates.join(','));
  }

  getValue(){
      return this.cell.getAssignedValue(); 
  }

}
