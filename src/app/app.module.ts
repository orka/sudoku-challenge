import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoardComponent } from './board/board.component';
import { BoardSectionComponent } from './board-section/board-section.component';
import { BoardCellComponent } from './board-cell/board-cell.component';

@NgModule({
  declarations: [
    AppComponent,
    BoardComponent,
    BoardSectionComponent,
    BoardCellComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
