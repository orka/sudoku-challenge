/**
 * Unit class component
 * Uses cells as its constituents
 * Unit can be a row, column or 3x3 grid section
 * see sudoku rules
 */
import Cell from './cell';

export default class Unit {
    cells: Array<any> = [];
    type: String = '';
    position: number;

    constructor(cells: Array<Cell>, type: String, position: number) {
        this.type = type;
        this.position = position;
        this.cells = cells;
    }

    hasCell(cell: Cell) {
        return this.cells.indexOf(cell) >= 0;
    }

    getPossibleValues() {
        const collection: Array<number> = [];
        const excludeCollection: Array<number> = [];
        // for cells with only one candidate - throw values into exclusion collection
        // for the rest
        this.cells.forEach((cell: Cell, i: number) => {
            if (cell.value > 0) {
                excludeCollection.push(cell.value);
            }
            // always push to collection incremental value to account for all possibilities
            collection.push(i + 1);
        });
        // from full list of values exclude the ones that are taken
        return collection.filter(v => excludeCollection.indexOf(v) < 0);
    }


    isFull() {
        // Carl Gauss's formula, (n / 2)(first number + last number) = sum
        return (
            this.getTotal() ===
            (this.cells.length / 2) *
            (1 + this.cells.length)
        );
    }

    getTotal() {
        return this.cells
            .map(v => v.value)
            .reduce((c: number, a: number) => {
                return c + a;
            });
    }


}