/**
 * Cell class component
 * Uses Unit as its parent component
 * Cell is a scare of a grid
 * see sudoku rules
 * 
 * Sell contains api to solve its given value candidates
 */
import Unit from './unit';

export default class Cell {
    position: number;
    units: Array<Unit> = [];
    peers: Array<Cell> = [];
    //
    value: number; // if value is 0 it was not defined initially
    candidates: Array<number> = [];
    tempCandidates: Array<number>;
    candidateIndex = 0;

    constructor(position: number) {
        this.position = position;
    }

    setValue(value: number) {
        if (value < 0 || value > 9) {
            throw new Error('Expected value from 0 to 9');
        }
        this.value = value;
        this.candidateIndex = 0;
    }

    getAssignedValue() {
        return this.value;
        return this.value > 0 ? this.value : this.tempCandidates && this.tempCandidates.length === 1 ? this.tempCandidates[0] : 0;
    }

    addUnit(unit: Unit) {
        this.units.push(unit);
        // add peers
        unit.cells.forEach(cell => {
            if (cell !== this && this.peers.indexOf(cell) < 0) {
                this.peers.push(cell);
            }
        });
    }

    hasCandidate(value: number) {
        return this.getCandidateIndex(value) >= 0;
    }

    isLastCandidate() {
        return this.tempCandidates.length === 1;
    }

    getCandidateIndex(value: number): number {
        return this.tempCandidates.indexOf(value);
    }

    /**
     * Eliminates a given value from its candidates
     * at the same time ensuring there is no conflict with its peers and units
     * @param value candidate to be removed
     * @returns true if candidate eliminated
     */
    eliminateCandidate(value: number): Boolean {
        if (this.hasCandidate(value) === false) {
            // no candidate found - return true as a successful operation
            return true;
        }

        if (this.isLastCandidate() === true) {
            // if its a last candidate - do not remove
            return false;
        }

        // if more than one candidate - remove
        this.tempCandidates.splice(this.getCandidateIndex(value), 1);

        if (this.isLastCandidate() === true) {
            // if after removing candidate - only one left - eliminate from all peers
            const lastCandidate = this.tempCandidates[0];

            for (let i = 0; i < this.peers.length; i += 1) {
                const peer = this.peers[i];
                //
                if (peer.eliminateCandidate(lastCandidate) === false) {
                    // elimination failed
                    return false;
                }
            }
        }

        //  check if the unit has positions for value we're eliminating
        //  if not - we cannot eliminate the value
        for (let i = 0; i < this.units.length; i += 1) {
            const unit = this.units[i];
            const unitCellWithValueCandidate: Array<Cell> = [];
            //  search unit cells for candidates - if no cells with such candidates - value cannot be eliminated
            for (let j = 0; j < unit.cells.length; j += 1) {
                const unitCell = unit.cells[j];
                //
                if (unitCell.hasCandidate(value) === true) {
                    //  eliminated value is possible elsewhere peer units
                    unitCellWithValueCandidate.push(unitCell);
                }
            }
            // after it was determined what possible positions for a give value unit has
            // check how many cells we found
            if (unitCellWithValueCandidate.length === 0) {
                //  eliminated value is not possible anywhere in cell units
                // therefore elimination is invalid
                return false;
            }

            // if only one possibility left assign value to that cell
            if (unitCellWithValueCandidate.length === 1) {
                const cell = unitCellWithValueCandidate[0];
                if (cell.assignPossibleCandidate(value) === false) {
                    // if value assignment failed - elimination is invalid
                    return false;
                }
            }
        }

        // elimination is successful ;)
        return true;
    }

    assignPossibleCandidate(value: number): Boolean {
        //
        for (let i = 0; i < this.tempCandidates.length; i += 1) {
            const eliminateValue = this.tempCandidates[i];
            // of other candidate cannot be illuminated return false
            if (eliminateValue !== value && this.eliminateCandidate(eliminateValue) === false) {
                return false;
            }
        }

        return true;
    }

    resetCandidates() {
        this.tempCandidates = this.candidates.slice(0);
    }

    setCandidates() {
        let collection = [];
        // if cell has an original value - set as candidate
        if (this.value > 0) {
            collection = [this.value];
        } else {
            //
            this.units.forEach(unit => {
                const unitPossibleValues = unit.getPossibleValues();
                // if no values set - assign possibilities
                if (collection.length === 0) {
                    collection.push(...unitPossibleValues);
                } else {
                    // next exclude possibilities from candidates that are not possible in other units
                    collection = collection.filter(
                        value => unitPossibleValues.indexOf(value) >= 0
                    );
                }
            });

        }
        this.candidates = collection;
        this.resetCandidates();
    }
}