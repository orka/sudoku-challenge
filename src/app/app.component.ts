import { Component, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'; 
import * as config from './config.json';
import { Board } from './board';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

@NgModule({
    imports: [
        FormsModule,
        NgbModule
    ]
})

export class AppComponent {
    title = config.title;
    author = config.author;
    board = [];
    solution = null;
    boardInitial = null;
    currentData: string = null;

    constructor() {
        this.boardInitial = new Board();
        this.solution = new Board();

        this.assignData(config.input[Math.floor(Math.random() * config.input.length)]);
    }

    assignData(data: string) {
        this.currentData = data;
        this.boardInitial.setData(this.currentData);
    }

    onData(event) {
        event.preventDefault();
        this.currentData = event.target[0].value;
        this.boardInitial.setData(this.currentData);
        this.solution.clearData();
    }

    solve() {
        // set initial board candidates
        this.boardInitial.setCandidates();
        this.solution.setData(this.currentData);
        this.solution.solve();
    }

    candidates() {
        this.boardInitial.setCandidates();
    }
}
